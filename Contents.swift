//: Playground - noun: a place where people can play

import UIKit

class Int16uBytes2uUnion {
    private var array = [UInt8](repeating: 0, count: 2)
    var int16uVal: UInt16 = 0
    var byte8uVals2: [UInt8] {
        set{
                array[0] = newValue[0]
                array[1] = newValue[1]
                int16uVal =  ( UInt16(array[1]) * 256 ) + UInt16(array[0])
        }
        
        get{
            array[0] = UInt8(int16uVal & 0x00FF)
            array[1] = UInt8( (int16uVal >> 8) & 0xFF )
            return array
        }
    }
}


// Simple tests
var x = Int16uBytes2uUnion()

x.int16uVal = 6115
print(x.int16uVal)
print(x.byte8uVals2)
print(x.byte8uVals2[0])


var array = [UInt8](repeating: 0, count: 2)
array[0]=226
array[1]=23
x.byte8uVals2=array
print(x.int16uVal)



x.byte8uVals2[0]=227
x.byte8uVals2[1]=24
print(x.int16uVal)
